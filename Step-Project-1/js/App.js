const serviceBtn = document.querySelectorAll(".service-link");
const serviceItem = document.querySelectorAll(".info-services");
serviceBtn.forEach(function (item) {
	item.addEventListener("click", function () {
		let currentBtn = item;
		let tabId = currentBtn.getAttribute("data-tab");
		let currentTab = document.querySelector(tabId);
		if (!currentBtn.classList.contains("active")) {
			serviceBtn.forEach(function (item) {
				item.classList.remove("active");
			});
			serviceItem.forEach(function (item) {
				item.classList.remove("active");
			});
			currentBtn.classList.add("active");
			currentTab.classList.add("active");
		}
	});
});
document.querySelector(".service-link").click();


function tabsFilter() {
	const cardsFilter = document.querySelectorAll(".cards-item-image");
	const bt = document.querySelectorAll(".work-link");
	bt.forEach((item) => {
		item.addEventListener("click", () => {
			let btn = item;
			if (!btn.classList.contains("activate")) {
				bt.forEach((item) => {
					item.classList.remove("activate");
				});
				btn.classList.add("activate");
			}
		});
	});
	document.querySelector(".work-list").addEventListener("click", (event) => {
		if (event.target.tagName !== "LI") { return false };
		let classFilter = event.target.dataset["f"];
		cardsFilter.forEach((elem) => {
			elem.classList.remove("hide");
			if (!elem.classList.contains(classFilter) && classFilter !== "all") {
				elem.classList.add("hide");
			}
		})
	});
}
tabsFilter();

const btnLoadMore = $('.cards-button');
const div = $('.cards-item-image.hidden-item');
btnLoadMore.on("click", event => {

	btnLoadMore.remove();

	const firstHiddenItems = $.grep(div, (item, index) => {
		if (index < 12) return true;
	});

	$(firstHiddenItems).removeClass("hidden-item");
})

function slider() {
	const btnPrev = document.querySelector('.btn-prev'),
		btnNext = document.querySelector('.btn-next'),
		slides = document.querySelectorAll('.people-card'),
		dots = document.querySelectorAll('.icones');
	let index = 0;
	const activeSlide = n => {
		for (const slide of slides) {
			slide.classList.remove('activs')
		}
		slides[n].classList.add('activs')
	}
	const activeDot = n => {
		for (const dot of dots) {
			dot.classList.remove('active')
		}
		dots[n].classList.add('active')
	}
	const prepareCurrentSlide = ind => {
		activeSlide(ind);
		activeDot(ind);
	}
	const nextSlide = () => {
		if (index === slides.length - 1) {
			index = 0;
			prepareCurrentSlide(index)
		} else {
			index++;
			prepareCurrentSlide(index)
		}
	}
	const prevSlide = () => {
		if (index === 0) {
			index = slides.length - 1;
			prepareCurrentSlide(index)
		} else {
			index--;
			prepareCurrentSlide(index)
		}
	}
	dots.forEach((item, indexDot) => {
		item.addEventListener('click', () => {
			index = indexDot
			prepareCurrentSlide(index)
		})
	})
	btnNext.addEventListener('click', nextSlide)
	btnPrev.addEventListener('click', prevSlide)
}
slider();